CREATE TABLE articulo (id BIGINT, categoria_id BIGINT, nombrearticulo VARCHAR(255), descripcionarticulo VARCHAR(255), cantidad BIGINT, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX categoria_id_idx (categoria_id), PRIMARY KEY(id)) ENGINE = INNODB;
CREATE TABLE categoria (id BIGINT AUTO_INCREMENT, nombrecategoria VARCHAR(255), descripcioncategoria VARCHAR(255), created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id)) ENGINE = INNODB;
CREATE TABLE entrada (id BIGINT AUTO_INCREMENT, articulo_id BIGINT, fechacompra DATE, cantidad BIGINT, preciocompra BIGINT, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX articulo_id_idx (articulo_id), PRIMARY KEY(id)) ENGINE = INNODB;
CREATE TABLE salida (id BIGINT AUTO_INCREMENT, articulo_id BIGINT, fechaventa DATE, cantidad BIGINT, precioventa BIGINT, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX articulo_id_idx (articulo_id), PRIMARY KEY(id)) ENGINE = INNODB;
ALTER TABLE articulo ADD CONSTRAINT articulo_categoria_id_categoria_id FOREIGN KEY (categoria_id) REFERENCES categoria(id) ON DELETE CASCADE;
ALTER TABLE entrada ADD CONSTRAINT entrada_articulo_id_articulo_id FOREIGN KEY (articulo_id) REFERENCES articulo(id) ON DELETE CASCADE;
ALTER TABLE salida ADD CONSTRAINT salida_articulo_id_articulo_id FOREIGN KEY (articulo_id) REFERENCES articulo(id) ON DELETE CASCADE;
