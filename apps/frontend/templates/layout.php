<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <?php include_http_metas() ?>
    <?php include_metas() ?>
    <?php include_title() ?>
    <link rel="shortcut icon" href="/favicon.ico" />
    <?php include_stylesheets() ?>
    <?php include_javascripts() ?>
  </head>
  <body>
    <div id="Encabezado">
       
        <div><?php echo image_tag('tecnoshop.png',array('alt' => 'Alternative text', 'size' => '200x200'))  ?></div>
        
        <div><h1 id="TituloTecno">TecnoShop</h1></div>
    </div>
    <div id="Contenido">
         <?php echo $sf_content ?>
    </div>
    <div id="Footer">
      <h2>WWW.TECNOSHOP.COM</h2>
      <h2>LUISA MARIA MARIN MUÑOZ</h2>
      <h2>DERECHOS DE AUTOR</h2>
    </div>
   
  </body>
</html>
